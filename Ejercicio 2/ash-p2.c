#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include "pokemon.h"
#include <time.h>

#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"

char *args[] = {"pokemon", "pokemon", NULL};
char *args2[] = {"pokedex", "pokedex", NULL};

#define MAX 100
enum State
{
  WaitingPokedex = 1,
  WaitingPokemon = 2,
  Fighting = 3,
  EndFight = 4
};
enum State currentStatus;

void pokedex()
{
  currentStatus = WaitingPokemon;
}

void acabar(){
  char msg[200];
  sprintf(msg,"El programa ha rebut la senyal (12 o 2)...\n");
  write(1,msg,strlen(msg));
  sprintf(msg,"Acabant....\n");
  write(1,msg,strlen(msg));
  exit(0);
}

int main(int arc, char *arv[])
{
  signal(SIGUSR1, pokedex);

  srand(time(NULL));
  int pokemonId;
  int pipe1[2], pipe2[2], pipe3[2];
  int pid1, pid2;
  char str[MAX];
  int endFlag = 1;

  if (pipe(pipe1) < 0)
  {
    perror("Error create pipe1");
    exit(-1);
  }

  if (pipe(pipe2) < 0)
  {
    perror("Error create pipe2");
    exit(-1);
  }

  if (pipe(pipe3) < 0)
  {
    perror("Error create pipe2");
    exit(-1);
  }

  switch (pid1 = fork())
  {
    case -1:
    perror("Error creating children\n");
    exit(-1);
    case 0:
    close(0);
    dup(pipe1[0]);
    close(1);
    dup(pipe2[1]);
    close(pipe1[0]);
    close(pipe1[1]);
    close(pipe2[0]);
    close(pipe2[1]);
    execv(args2[0], args2);
    exit(0);
  }
  currentStatus = WaitingPokedex;
  while (currentStatus == WaitingPokedex)
  {
  }

  while (endFlag == 1)
  {

    char s[100];
    char choice;

    sprintf(s, "################\n# E. Explore \n# Q. Quit\n################\n");
    if (write(1, s, strlen(s)) < 0)
    perror("Error writting the menu");
    scanf(" %c", &choice);

    switch (choice)
    {
      case 'Q':
      endFlag = 0;
      kill(pid1, SIGKILL); //Això ho hem deixat igual perquè amb la Q ja acaba el programa. 
      wait(NULL);
      sprintf(s, "%s!!!!I'm tired from all the fun... %s\n", KMAG, KNRM);
      write(1, s, strlen(s)); //Hem afegit un write perquè imprimeixi el missatge corresponent. 
      exit(0);
      break;
      case 'E':
      switch (pid2 = fork())
      {
        case 0:
        close(1);
        dup(pipe3[1]);
        close(pipe1[0]);
        close(pipe1[1]);
        close(pipe2[0]);
        close(pipe2[1]);
        close(pipe3[0]);
        close(pipe3[1]);
        execv(args[0], args);
        exit(0);
      }
      close(pipe3[1]);

      pokemonId = rand() % 151 + 1;
      write(pipe1[1], &pokemonId, sizeof(int));
      struct pokemon pokemon;
      read(pipe2[0], &pokemon, sizeof(pokemon));
      printf("%s\n", pokemon.name);
      fflush(stdout);

      break;
      default:
      sprintf(s, "%s!!!!Invalid option. Try again. %s\n", KRED, KNRM);
      if (write(1, s, strlen(s)) < 0)
      perror("Error writting invalid option");
    }
    currentStatus = Fighting;
    while (currentStatus == Fighting)
    {
      char choice2;
      int num;

      sprintf(s, "################\n# P. Throw pokeball \n# R. Run\n################\n");
      if (write(1, s, strlen(s)) < 0)
      perror("Error writting the menu");
      scanf(" %c", &choice2);
      //read(0, choice2, sizeof(char));
      switch (choice2)
      {
        case 'P':
        read(pipe3[0], &num, sizeof(num));
        switch (num)
        {
          case 7:
          printf("\nPokemon escaped");
          //fflush(stdout);
          currentStatus = EndFight;
          break;
          case 2:
          printf("\nPokemon was caught");
          //fflush(stdout);
          currentStatus = EndFight;
          break;
          default:
          printf("\nPokemon broke free");
          //fflush(stdout);
          break;

          signal(SIGINT, acabar);
          wait(NULL);
        }
        break;
        case 'R':
        kill(pid2, SIGKILL); //passa igual que amb la Q
        wait(NULL);
        currentStatus = EndFight;
        break;
      }
    }
  }
  char s[100];
  sprintf(s, "%s!!!!I'm tired from all the fun... %s\n", KMAG, KNRM);
  if (write(1, s, strlen(s)) < 0)
  perror("Error writting the ending msg");
  exit(0);
}