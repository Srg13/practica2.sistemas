#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include "packet.h"
#include "pokemon.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

char *args2[] = {"pokedex", "pokedex", NULL};

#define TRUE 1
#define FALSE 0
#define MAX 100

enum State {WaitingPokedex=1, WaitingPokemon=2, Fighting=3, EndFight=4};
enum State currentStatus;

int state_pipe;

void init_str(char str[MAX]);
void g_signal(int sig);
void pokedex(){
    currentStatus=WaitingPokemon;
}

int main(int arc, char *arv[]) {
    signal(SIGUSR1, pokedex);

    int pokemonId;
    int pipe1[2],pipe2[2];
    int pid1;
    state_pipe=0;
    char str [MAX];
    char buffer[4];

    if(pipe(pipe1)<0){
        perror("Error create pipe1");
        exit(-1);
    }

    if(pipe(pipe2)<0){
        perror("Error create pipe2");
        exit(-1);
    }
    
    while(1) {
        printf ("Enter a pokemonId between (1-151): ");
        scanf ("%d",&pokemonId);
        //snprintf(buffer, 4, "%d", pokemonId);
        switch (pid1=fork()) {
            case -1:
                perror("Error creating children\n");
                exit(-1);
            case 0:
                //read(0, buffer, sizeof(buffer));
                close(0);
                dup(pipe1[0]);
                close(1);
                dup(pipe2[1]);
                close(pipe1[0]);
                close(pipe1[1]);
                close(pipe2[0]);
                close(pipe2[1]);
                execv(args2[0], args2);
                exit(0);
        }
        currentStatus=WaitingPokedex;
        while(currentStatus==WaitingPokedex){}
        write(pipe1[1], &pokemonId, sizeof(int));
        struct pokemon pokemon;
        read(pipe2[0], &pokemon, sizeof(pokemon));
        printf("%s\n", pokemon.name);fflush(stdout);
        wait(NULL);

    }

        exit(0);
}