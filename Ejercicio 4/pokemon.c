#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include <time.h>

//cració de la funció acabar perquè pugués finalitzar el bucle infinit. 
void acabar(){
  char msg[200];
  sprintf(msg,"El programa ha rebut la senyal (12o 2)...\n");
  write(1,msg,strlen(msg));
  sprintf(msg,"Acabant....\n");
  write(1,msg,strlen(msg));
  exit(0);
}

int main ()
{
  srand(time(NULL));
  while(1){
  int num= rand()%10 +1;
  write(1, &num, sizeof(num));
  signal(SIGINT, acabar); //tractament de la senyal perquè pugui acabar el procés. 
  } 
  exit(0);

}
