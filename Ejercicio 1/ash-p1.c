#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>
#include "packet.h"
#include "pokemon.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

char *args2[] = {"pokedex", "pokedex", NULL};

#define TRUE 1
#define FALSE 0
#define MAX 100

enum State {WaitingPokedex=1, WaitingPokemon=2, Fighting=3, EndFight=4};
enum State currentStatus;

int state_pipe;

void init_str(char str[MAX]);
void g_signal(int sig);
void pokedex(){
    currentStatus=WaitingPokemon;
}

int main(int arc, char *arv[]) {
    signal(SIGUSR1, pokedex);

    int pokemonId;
    int pipe1[2],pipe2[2];
    int pid1;
    state_pipe=0;
    char str [MAX];
    char buffer[4];

    if(pipe(pipe1)<0){
        perror("Error create pipe1");
        exit(-1);
    }

    if(pipe(pipe2)<0){
        perror("Error create pipe2");
        exit(-1);
    }

    //@Jordi: S'han de crear 2 pipes: 
    //        1) Pare escriu el id del pokemon llegit de l'usuari i pokedex el llegeix de la pipe per la stdout(0).
    //        2) Fill escriu la informació del pokemon i el pare la mostrà per stdout (0)

    //@Jordi: Primer has de crear el fill amb el fork
    //pid=fork();
    //if (pid==0) {
            // Codi del fill
            //@Jordi: Un cop el fill creat has de:
                // Tancar les pipes que no necessitis.
                //        1) Redireccionar la stdin (0) a la pipe1[0]
                //        2) Redireccionar la stdout (1) a la pipe2[1]
                //        3) Fer el recobriment
    //}

    //@Jordi: Tancar les pipes que pare no necessitarà ja pipe1[0] i pipe2[1].
    // Nota que el pare necessitarà la pipe1[1] per enviar el idPokemon a la pokedex.
    // Nota que el pare necessitarà la pipe2[0] per llegir la informació que li enviarà la pokedex.


    //@Jordi: El pare no pot continuar..., ha d'esperar que el fill tingui carregada la pokedex en Memòria.
    //        Per exemple: Esperem a rebre SIG1

    // Podem utilitzar la variable currentStatus, ara heu d'actualitzar la variable currentStatus a WaitingPokemon quan es reb SIG1 del fill.
    /*currentStatus = WaitingPokedex;
    while(currentStatus == WaitingPokedex) {

    }*/

    //@Jordi; S'ha de fer un bucle infinit fins que el pare rebi la senyal SIGINT (ctrl-c), 
    //        en aquest punt s'ha d'enviar un SIG1 al fill, i esperar que el fill acabi, per acabar el pare.
    // En aquest bucle infinit:
    //         1) Demanar un pokemon [ok]
    
    while(1) {
        printf ("Enter a pokemonId between (1-151): ");
        scanf ("%d",&pokemonId);
        //snprintf(buffer, 4, "%d", pokemonId);
        switch (pid1=fork()) {
            case -1:
                perror("Error creating children\n");
                exit(-1);
            case 0:
                //read(0, buffer, sizeof(buffer));
                close(0);
                dup(pipe1[0]);
                close(1);
                dup(pipe2[1]);
                close(pipe1[0]);
                close(pipe1[1]);
                close(pipe2[0]);
                close(pipe2[1]);
                execv(args2[0], args2);
                exit(0);
        }
        currentStatus=WaitingPokedex;
        while(currentStatus==WaitingPokedex){}
        write(pipe1[1], &pokemonId, sizeof(int));
        struct pokemon pokemon;
        read(pipe2[0], &pokemon, sizeof(pokemon));
        printf("%s\n", pokemon.name);fflush(stdout);
        wait(NULL);

        // Llegir de la pipe2 la informació del pokemon.

        // Escriure la informació del pokemon per stdout(1)

    }

        exit(0);
}
